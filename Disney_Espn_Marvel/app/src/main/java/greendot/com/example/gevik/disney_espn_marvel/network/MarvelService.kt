package greendot.com.example.gevik.disney_espn_marvel.network

import greendot.com.example.gevik.disney_espn_marvel.network.model.ComicResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface MarvelService {
    @GET("comics/{comicId}")
    fun getComics(
        @Path("comicId") id: Int
    ): Single<ComicResponse>
}