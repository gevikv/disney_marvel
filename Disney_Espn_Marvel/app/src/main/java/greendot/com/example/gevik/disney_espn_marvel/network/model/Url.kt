package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Url(
    val type: String?,
    val url: String?
)