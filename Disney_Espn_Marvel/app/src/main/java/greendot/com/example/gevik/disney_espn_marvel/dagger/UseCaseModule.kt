package greendot.com.example.gevik.disney_espn_marvel.dagger

import dagger.Module
import dagger.Provides
import greendot.com.example.gevik.disney_espn_marvel.domain.MarvelFeedUseCase
import greendot.com.example.gevik.disney_espn_marvel.domain.MarvelFeedUseCaseImp
import greendot.com.example.gevik.disney_espn_marvel.network.Repository

@Module
class UseCaseModule {
    @Provides
    fun provideMarvelFeedUseCase(repository: Repository): MarvelFeedUseCase {
        return MarvelFeedUseCaseImp(repository)
    }
}