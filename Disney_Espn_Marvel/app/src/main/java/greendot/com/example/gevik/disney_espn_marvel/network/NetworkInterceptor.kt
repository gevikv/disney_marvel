package greendot.com.example.gevik.disney_espn_marvel.network

import greendot.com.example.gevik.disney_espn_marvel.Constants
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class NetworkInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val ts = System.currentTimeMillis().toString()
        val apikey = Constants.API_KEY  //public api key
        val hashInput = "$ts${Constants.PRIVATE_KEY}${Constants.API_KEY}"

        val httpUrl = originalRequest.url.newBuilder()
            .addQueryParameter(TS, ts)
            .addQueryParameter(API_KEY, apikey)
            .addQueryParameter(HASH, hashInput.md5()).build()

        val builder = originalRequest.newBuilder().url(httpUrl)
        val newRequest = builder.build()
        return chain.proceed(newRequest)
    }

    companion object {
        private val TS = "ts"
        private val API_KEY = "apikey"
        private val HASH = "hash"
    }
}