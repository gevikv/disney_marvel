package greendot.com.example.gevik.disney_espn_marvel.features.marvelfeed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import greendot.com.example.gevik.disney_espn_marvel.domain.MarvelFeedUseCase
import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import greendot.com.example.gevik.disney_espn_marvel.network.model.State
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class MarvelFeedViewModel @Inject constructor(private val useCase: MarvelFeedUseCase) :
    ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private var _comicResponse: MutableLiveData<State> = MutableLiveData()
    val comicResponse: LiveData<State> = _comicResponse
    fun getComic(id: Int) {
        useCase.getComic(id).subscribeWith(object : DisposableSingleObserver<Comic>() {
            override fun onSuccess(response: Comic) {
                _comicResponse.postValue(State.Result(response))
            }

            override fun onError(e: Throwable) {
                _comicResponse.postValue(State.Error(e.toString()))
            }
        }).addTo(compositeDisposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}