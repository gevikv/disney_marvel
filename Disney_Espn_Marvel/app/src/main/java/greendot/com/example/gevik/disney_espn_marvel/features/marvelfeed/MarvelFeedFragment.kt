package greendot.com.example.gevik.disney_espn_marvel.features.marvelfeed

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import greendot.com.example.gevik.disney_espn_marvel.AppApplication
import greendot.com.example.gevik.disney_espn_marvel.R
import greendot.com.example.gevik.disney_espn_marvel.ViewModelFactory
import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import greendot.com.example.gevik.disney_espn_marvel.network.model.State
import greendot.com.example.gevik.disney_espn_marvel.util.EspressoIdlingResource
import kotlinx.android.synthetic.main.fragment_marvel_feed.*
import javax.inject.Inject

class MarvelFeedFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory<MarvelFeedViewModel>

    private val viewModel: MarvelFeedViewModel by lazy {
        viewModelFactory.get<MarvelFeedViewModel>(
            requireActivity()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.getAppComponent().inject(this)
        viewModel.getComic(1308)  //spiderMan ID
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_marvel_feed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        EspressoIdlingResource.increment()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.comicResponse.observe(viewLifecycleOwner) {
            when (it) {
                is State.Result -> {
                    initUiValues(it.result)
                    progressBar?.visibility = View.GONE
                    EspressoIdlingResource.decrement()
                }
                is State.Error -> {
                    Toast.makeText(activity, getString(R.string.error), Toast.LENGTH_SHORT).show()
                    progressBar?.visibility = View.GONE
                }
                is State.Loading -> {

                }
            }
        }
    }

    private fun initUiValues(response: Comic) {
        val imagePath = response.imageUrl
        val imageExt = response.imageExt
        val imageUrl = "$imagePath.$imageExt"
        Glide.with(this).load(imageUrl).placeholder(R.drawable.default_image)
            .error(R.drawable.default_image).into(image_cover)
        title_value.text = response.title
        description_value.text = response.description
    }

}