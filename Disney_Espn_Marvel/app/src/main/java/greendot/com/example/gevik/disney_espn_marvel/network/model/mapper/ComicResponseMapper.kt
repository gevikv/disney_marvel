package greendot.com.example.gevik.disney_espn_marvel.network.model.mapper

import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import greendot.com.example.gevik.disney_espn_marvel.network.model.ComicResponse

class ComicResponseMapper : Mapper<Comic, ComicResponse> {
    override fun mapToEntity(comicResponse: ComicResponse): Comic {
        val result = comicResponse.data?.results?.get(0)  //getting the first value since it is list of objects
        val image = result?.images?.get(0)
        return Comic(
            title = result?.title, description = result?.description,
            imageUrl = image?.path, imageExt = image?.extension
        )
    }
}
