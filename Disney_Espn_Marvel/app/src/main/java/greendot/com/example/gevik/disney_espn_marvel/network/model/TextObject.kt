package greendot.com.example.gevik.disney_espn_marvel.network.model

data class TextObject(
    val language: String?,
    val text: String?,
    val type: String?
)