package greendot.com.example.gevik.disney_espn_marvel.network.model

sealed class State{
    data class Result(val result:Comic) : State()
    data class Error(val error:String) : State()
    data class Loading(val isLoading:Boolean): State()
}
