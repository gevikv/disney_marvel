package greendot.com.example.gevik.disney_espn_marvel

import android.app.Application
import greendot.com.example.gevik.disney_espn_marvel.dagger.AppComponent
import greendot.com.example.gevik.disney_espn_marvel.dagger.AppModule
import greendot.com.example.gevik.disney_espn_marvel.dagger.DaggerAppComponent

class AppApplication : Application() {
    companion object {
        private lateinit var appComponent: AppComponent

        fun getAppComponent(): AppComponent {
            return appComponent
        }
    }

    override fun onCreate() {
        super.onCreate()
        initDaggerAppComponent()
    }

    private fun initDaggerAppComponent(): AppComponent {
        appComponent =
            DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        return appComponent
    }

}