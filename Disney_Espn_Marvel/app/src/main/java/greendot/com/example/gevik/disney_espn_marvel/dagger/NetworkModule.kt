package greendot.com.example.gevik.disney_espn_marvel.dagger

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import greendot.com.example.gevik.disney_espn_marvel.Constants
import greendot.com.example.gevik.disney_espn_marvel.network.MarvelService
import greendot.com.example.gevik.disney_espn_marvel.network.NetworkInterceptor
import greendot.com.example.gevik.disney_espn_marvel.network.Repository
import greendot.com.example.gevik.disney_espn_marvel.network.RepositoryImp
import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import greendot.com.example.gevik.disney_espn_marvel.network.model.ComicResponse
import greendot.com.example.gevik.disney_espn_marvel.network.model.mapper.ComicResponseMapper
import greendot.com.example.gevik.disney_espn_marvel.network.model.mapper.Mapper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {
    @Provides
    fun provideTPSService(okHttpClient: OkHttpClient): MarvelService {
        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(Constants.BASE_URL)
            .client(okHttpClient)
            .build()
        return retrofit.create(MarvelService::class.java)
    }

    @Provides
    fun provideOkHttpClient(
        networkInterceptor: NetworkInterceptor
    ): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        okHttpClientBuilder.addInterceptor(networkInterceptor)
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
        return okHttpClientBuilder.build()
    }

    @Provides
    fun providesRepository(
        service: MarvelService,
        mapper: Mapper<Comic, ComicResponse>
    ): Repository {
        return RepositoryImp(service, mapper)
    }

    @Provides
    fun providesComicMapper(): Mapper<Comic, ComicResponse> {
        return ComicResponseMapper()
    }
}