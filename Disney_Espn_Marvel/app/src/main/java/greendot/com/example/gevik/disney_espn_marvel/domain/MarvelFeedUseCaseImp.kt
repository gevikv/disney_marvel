package greendot.com.example.gevik.disney_espn_marvel.domain

import greendot.com.example.gevik.disney_espn_marvel.network.Repository
import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MarvelFeedUseCaseImp @Inject constructor(private val repository: Repository) :
    MarvelFeedUseCase {
    override fun getComic(id: Int): Single<Comic> {
        return repository.getComic(id).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}