package greendot.com.example.gevik.disney_espn_marvel.dagger

import dagger.Component
import greendot.com.example.gevik.disney_espn_marvel.AppApplication
import greendot.com.example.gevik.disney_espn_marvel.features.marvelfeed.MarvelFeedFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, UseCaseModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        fun appModule(module: AppModule): Builder
        fun build(): AppComponent
    }

    fun inject(app: AppApplication)
    fun inject(marvelFeedFragment: MarvelFeedFragment)
}