package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Image(
    val extension: String?,
    val path: String?
)