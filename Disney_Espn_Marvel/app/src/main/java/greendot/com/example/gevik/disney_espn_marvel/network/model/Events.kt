package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Events(
    val available: Int?,
    val collectionURI: String?,
    val items: List<Any>?,
    val returned: Int?
)