package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Creators(
    val available: Int?,
    val collectionURI: String?,
    val items: List<ItemX>?,
    val returned: Int?
)