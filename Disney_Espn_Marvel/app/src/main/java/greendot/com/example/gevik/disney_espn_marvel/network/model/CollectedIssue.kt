package greendot.com.example.gevik.disney_espn_marvel.network.model

data class CollectedIssue(
    val name: String?,
    val resourceURI: String?
)