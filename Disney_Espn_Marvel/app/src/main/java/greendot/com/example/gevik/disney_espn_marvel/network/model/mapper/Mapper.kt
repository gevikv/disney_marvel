package greendot.com.example.gevik.disney_espn_marvel.network.model.mapper

interface Mapper<E, D> {
    fun mapToEntity(type: D): E
}