package greendot.com.example.gevik.disney_espn_marvel.network

import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import greendot.com.example.gevik.disney_espn_marvel.network.model.ComicResponse
import greendot.com.example.gevik.disney_espn_marvel.network.model.mapper.Mapper
import io.reactivex.Single
import javax.inject.Inject

class RepositoryImp @Inject constructor(
    private val service: MarvelService,
    private val mapper: Mapper<Comic, ComicResponse>
) : Repository {
    override fun getComic(id: Int): Single<Comic> {
        return service.getComics(id).flatMap { Single.just(mapper.mapToEntity(it)) }
    }
}