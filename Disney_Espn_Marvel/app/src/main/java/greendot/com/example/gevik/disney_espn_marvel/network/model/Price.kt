package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Price(
    val price: Double?,
    val type: String?
)