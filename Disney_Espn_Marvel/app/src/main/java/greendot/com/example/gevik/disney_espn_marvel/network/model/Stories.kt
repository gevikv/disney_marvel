package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Stories(
    val available: Int?,
    val collectionURI: String?,
    val items: List<ItemXX>?,
    val returned: Int?
)