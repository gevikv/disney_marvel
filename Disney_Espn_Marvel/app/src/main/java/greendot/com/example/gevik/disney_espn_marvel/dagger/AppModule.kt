package greendot.com.example.gevik.disney_espn_marvel.dagger

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import greendot.com.example.gevik.disney_espn_marvel.AppApplication
import javax.inject.Singleton

@Module
class AppModule constructor(private val application: AppApplication) {
    @Provides
    @Singleton
    fun getApplication(): Application {
        return application
    }

    fun getContext(application: Application): Context {
        return application.applicationContext
    }
}