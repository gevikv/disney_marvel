package greendot.com.example.gevik.disney_espn_marvel.network.model

data class ItemXX(
    val name: String?,
    val resourceURI: String?,
    val type: String?
)