package greendot.com.example.gevik.disney_espn_marvel.network

import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import io.reactivex.Single

interface Repository {
    fun getComic(id: Int): Single<Comic>
}