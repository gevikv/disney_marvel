package greendot.com.example.gevik.disney_espn_marvel.domain

import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import io.reactivex.Single

interface MarvelFeedUseCase {
    fun getComic(id: Int): Single<Comic>
}