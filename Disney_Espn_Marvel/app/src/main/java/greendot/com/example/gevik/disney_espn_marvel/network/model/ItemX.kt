package greendot.com.example.gevik.disney_espn_marvel.network.model

data class ItemX(
    val name: String?,
    val resourceURI: String?,
    val role: String?
)