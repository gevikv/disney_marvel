package greendot.com.example.gevik.disney_espn_marvel.network.model

data class ComicResponse(
    val `data`: Data?
)