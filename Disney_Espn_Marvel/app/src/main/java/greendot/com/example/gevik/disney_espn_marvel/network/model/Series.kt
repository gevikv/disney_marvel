package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Series(
    val name: String?,
    val resourceURI: String?
)