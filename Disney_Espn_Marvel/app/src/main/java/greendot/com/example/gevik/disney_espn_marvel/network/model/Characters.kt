package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Characters(
    val available: Int?,
    val collectionURI: String?,
    val items: List<Item>?,
    val returned: Int?
)