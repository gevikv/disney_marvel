package greendot.com.example.gevik.disney_espn_marvel.network.model

data class Date(
    val date: String?,
    val type: String?
)