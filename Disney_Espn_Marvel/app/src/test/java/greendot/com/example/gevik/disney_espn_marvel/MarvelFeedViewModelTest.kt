package greendot.com.example.gevik.disney_espn_marvel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import greendot.com.example.gevik.disney_espn_marvel.domain.MarvelFeedUseCaseImp
import greendot.com.example.gevik.disney_espn_marvel.features.marvelfeed.MarvelFeedViewModel
import greendot.com.example.gevik.disney_espn_marvel.network.MarvelService
import greendot.com.example.gevik.disney_espn_marvel.network.RepositoryImp
import greendot.com.example.gevik.disney_espn_marvel.network.model.Comic
import greendot.com.example.gevik.disney_espn_marvel.network.model.State
import io.reactivex.Single
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit

class MarvelFeedViewModelTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()

    @Rule
    @JvmField
    var rxRule = RxJavaRule()

    @Rule
    @JvmField
    val instantTaskRule = InstantTaskExecutorRule()

    @Mock
    lateinit var marvelFeedUseCaseImp: MarvelFeedUseCaseImp

    @Test
    fun getUseCaseComicFunctionCalledTest() {
        val viewModel = MarvelFeedViewModel(marvelFeedUseCaseImp)
        viewModel.comicResponse.observeForever { }
        Mockito.`when`(marvelFeedUseCaseImp.getComic(1781)).thenReturn(Single.just(getComic()))
        viewModel.getComic(1781)
        verify(marvelFeedUseCaseImp, times(1)).getComic(1781)
    }

    @Test
    fun marvelFeedViewModelTest() {
        val viewModel = MarvelFeedViewModel(marvelFeedUseCaseImp)
        viewModel.comicResponse.observeForever { }
        Mockito.`when`(marvelFeedUseCaseImp.getComic(1781)).thenReturn(Single.just(getComic()))
        viewModel.getComic(1781)
        Assert.assertEquals(State.Result(getComic()), viewModel.comicResponse.value)
    }

    private fun getComic(): Comic {
        return Comic(
            title = "spider man",
            description = "spider man description",
            imageUrl = "http://url/spiderman/test",
            imageExt = "png"
        )
    }
}