# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for and what libraries are used? ###

* Quick summary
* this is to show how to use Marvel API 
* used libraries :Dagger ,Jetpack navigation ,RxJava ,Mockito ,Espresso ,Glide ,Retrofit ,Okhttp
* Design pattern : MVVM with Clean Architecture

### How do I get set up? ###

* Summary of set up
1: go to the https://developer.marvel.com/
2: register and get your private key and public key
3: on the Constants Class replace the API_KEY  with your public key and PRIVATE_KEY with your private key


### How to Download the app ###
On the left side of the panel ,click on the Download button,then click on Download repository

